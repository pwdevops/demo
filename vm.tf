resource "azurerm_public_ip" "lb_public_ip" {
  name                = "ip-${var.project_name}"
  location            = azurerm_resource_group.rg.location
  resource_group_name = azurerm_resource_group.rg.name
  allocation_method   = "Static"
}

resource "azurerm_lb" "lb" {
  name                = "lb-${var.project_name}"
  location            = azurerm_resource_group.rg.location
  resource_group_name = azurerm_resource_group.rg.name

  frontend_ip_configuration {
    name                 = "PublicIPAddress"
    public_ip_address_id = azurerm_public_ip.lb_public_ip.id
  }
}

resource "azurerm_lb_backend_address_pool" "backend_ip_pool" {
  loadbalancer_id = azurerm_lb.lb.id
  name            = "BackEndAddressPool"
}

resource "azurerm_lb_probe" "backend_lb_probe" {
  loadbalancer_id = azurerm_lb.lb.id
  name            = "ssh-running-probe-${var.project_name}"
  port            = local.ssh_port
}

resource "azurerm_lb_rule" "backend_lb_nat_rule" {
  loadbalancer_id                = azurerm_lb.lb.id
  name                           = "http"
  protocol                       = "Tcp"
  frontend_port                  = local.frontend_port
  backend_port                   = local.backend_port
  backend_address_pool_ids       = [azurerm_lb_backend_address_pool.backend_ip_pool.id]
  frontend_ip_configuration_name = "PublicIPAddress"
  probe_id                       = azurerm_lb_probe.backend_lb_probe.id
}

resource "azurerm_linux_virtual_machine_scale_set" "backend_vm_scale_set" {
  name = "vm-${var.project_name}"

  depends_on = [
    azurerm_key_vault_secret.vault_mysql_admin_password,
    azurerm_key_vault_secret.vault_mysql_admin_username,
    azurerm_key_vault_secret.vault_mysql_db_name,
    azurerm_key_vault_secret.vault_mysql_hostname,
    azurerm_key_vault_secret.vault_sas_url_query_string,
    azurerm_storage_blob.vm_config_storage_blob,
    azurerm_mysql_server.mysql_server
  ]

  resource_group_name = azurerm_resource_group.rg.name
  location            = azurerm_resource_group.rg.location
  sku                 = local.vm_size
  instances           = 1
  admin_username      = local.vm_username
  custom_data = base64encode(templatefile("${path.module}/templates/vm_user_data.sh.tpl", {
    vm_setup_url   = azurerm_storage_blob.vm_config_storage_blob["vm_setup.sh.tpl"].url
    key_vault_name = azurerm_key_vault.vault.name
  }))

  boot_diagnostics {
    storage_account_uri = azurerm_storage_account.storage_account.primary_blob_endpoint
  }

  admin_ssh_key {
    username   = local.vm_username
    public_key = jsondecode(azapi_resource_action.ssh_public_key_gen.output).publicKey
  }

  source_image_reference {
    publisher = local.vm_image_publisher
    offer     = local.vm_image_offer
    sku       = local.vm_image_sku
    version   = local.vm_image_version
  }

  os_disk {
    caching              = local.vm_disk_caching
    storage_account_type = local.vm_disk_acount_type
  }

  network_interface {
    name    = "nic-${var.project_name}"
    primary = true

    ip_configuration {
      name                                   = "internal"
      primary                                = true
      subnet_id                              = azurerm_subnet.subnet.id
      load_balancer_backend_address_pool_ids = [azurerm_lb_backend_address_pool.backend_ip_pool.id]
    }
  }

  identity {
    type = "SystemAssigned"
  }

}

data "azurerm_subscription" "current" {}

data "azurerm_role_definition" "contributor" {
  name = "Contributor"
}

resource "azurerm_role_assignment" "backend_vm_identity_role" {
  scope              = data.azurerm_subscription.current.id
  role_definition_id = "${data.azurerm_subscription.current.id}${data.azurerm_role_definition.contributor.id}"
  principal_id       = azurerm_linux_virtual_machine_scale_set.backend_vm_scale_set.identity[0].principal_id
}
