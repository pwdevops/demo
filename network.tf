resource "azurerm_resource_group" "rg" {
  location = local.region
  name     = "rg-${var.project_name}"
}

resource "azurerm_virtual_network" "virtual_network" {
  name                = "net-${var.project_name}"
  address_space       = ["10.0.0.0/16"]
  location            = azurerm_resource_group.rg.location
  resource_group_name = azurerm_resource_group.rg.name
}

resource "azurerm_subnet" "subnet" {
  name                 = "subnet-${var.project_name}"
  resource_group_name  = azurerm_resource_group.rg.name
  virtual_network_name = azurerm_virtual_network.virtual_network.name
  address_prefixes     = ["10.0.1.0/24"]
}

