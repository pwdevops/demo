#!/bin/bash
echo "VM Setup"

apt-get update

apt-get install -y \
  net-tools \
  nginx \
  zip \
  mysql-client

DEBIAN_FRONTEND=noninteractive apt-get install -y postfix

mkdir -p /data

az login --identity
export KEY_VAULT_NAME="${key_vault_name}"
export MYSQL_HOSTNAME=$(az keyvault secret show --name "mysql-hostname" --vault-name "$KEY_VAULT_NAME" --query "value" | sed 's/"//g')
export MYSQL_DB_NAME=$(az keyvault secret show --name "mysql-db-name" --vault-name "$KEY_VAULT_NAME" --query "value" | sed 's/"//g')
export MYSQL_ADMIN_USERNAME=$(az keyvault secret show --name "mysql-admin-username" --vault-name "$KEY_VAULT_NAME" --query "value" | sed 's/"//g')
export MYSQL_ADMIN_PASSWORD=$(az keyvault secret show --name "mysql-admin-pasword" --vault-name "$KEY_VAULT_NAME" --query "value" | sed 's/"//g')

export mysql_defaults_file="/root/.my.cnf"
cat << EOF > $mysql_defaults_file
[client]
host="$MYSQL_HOSTNAME"
database="$MYSQL_DB_NAME"
user="$MYSQL_ADMIN_USERNAME"
password="$MYSQL_ADMIN_PASSWORD"
EOF
chmod 0600 $mysql_defaults_file

timeout 600 bash -c 'until mysql --defaults-file=$mysql_defaults_file -e "SELECT VERSION()"; do sleep 10; echo "Waiting to ensure MySQL server available..." ;done'

mysql --defaults-file=$mysql_defaults_file << 'EOF'
CREATE DATABASE IF NOT EXISTS ${project}cfg;

USE ${project}cfg;

CREATE TABLE IF NOT EXISTS `users` (
  `id` int(11) NOT NULL auto_increment,
  `alias` varchar(100) NOT NULL,
  `description` varchar(100) NOT NULL,
  PRIMARY KEY  (`id`)
);
EOF

touch /tmp/bootstrap-finished.txt
