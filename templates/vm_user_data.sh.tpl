#!/bin/bash
set -euo pipefail
cat << 'EOF' > /usr/local/sbin/vm_bootstrap_setup.sh
#!/bin/bash
set -euo pipefail
curl -sL https://aka.ms/InstallAzureCLIDeb | sudo bash
az login --identity
export SAS_URL_QUERY_STRING=$(az keyvault secret show --name "sas-url-query-string" --vault-name "${key_vault_name}" --query "value" | sed 's/"//g')
set -euo pipefail
curl -o /usr/local/sbin/vm_setup.sh "${vm_setup_url}$SAS_URL_QUERY_STRING"
chmod 755 /usr/local/sbin/vm_setup.sh
/usr/local/sbin/vm_setup.sh
EOF
chmod 755 /usr/local/sbin/vm_bootstrap_setup.sh
/usr/local/sbin/vm_bootstrap_setup.sh
