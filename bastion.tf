resource "azurerm_public_ip" "bastion_public_ip" {
  name                = "bastion-ip-${var.project_name}"
  location            = azurerm_resource_group.rg.location
  resource_group_name = azurerm_resource_group.rg.name
  allocation_method   = "Static"
}

resource "azurerm_network_interface" "bastion_nic" {
  name                = "bastion-nic"
  location            = azurerm_resource_group.rg.location
  resource_group_name = azurerm_resource_group.rg.name

  ip_configuration {
    name                          = "IPConfiguration"
    subnet_id                     = azurerm_subnet.subnet.id
    private_ip_address_allocation = "Dynamic"
    public_ip_address_id          = azurerm_public_ip.bastion_public_ip.id
  }
}

resource "azurerm_network_security_group" "bastion_nsg" {
  name                = "bastion-${var.project_name}"
  location            = azurerm_resource_group.rg.location
  resource_group_name = azurerm_resource_group.rg.name

  security_rule {
    name                       = "SSH"
    priority                   = 1001
    direction                  = "Inbound"
    access                     = "Allow"
    protocol                   = "Tcp"
    source_port_range          = "*"
    destination_port_range     = local.bastion_ssh_port
    source_address_prefix      = "*"
    destination_address_prefix = "*"
  }
}

resource "azurerm_network_interface_security_group_association" "bastion_nsg_association" {
  network_interface_id      = azurerm_network_interface.bastion_nic.id
  network_security_group_id = azurerm_network_security_group.bastion_nsg.id
}

resource "azurerm_linux_virtual_machine" "vm_bastion" {
  name                  = "bastion-${var.project_name}"
  location              = azurerm_resource_group.rg.location
  resource_group_name   = azurerm_resource_group.rg.name
  network_interface_ids = [azurerm_network_interface.bastion_nic.id]
  size                  = local.vm_size

  os_disk {
    name                 = "os-disk-${var.project_name}"
    caching              = local.vm_disk_caching
    storage_account_type = local.vm_disk_acount_type
  }

  source_image_reference {
    publisher = local.vm_image_publisher
    offer     = local.vm_image_offer
    sku       = local.vm_image_sku
    version   = local.vm_image_version
  }

  computer_name  = "bastion"
  admin_username = local.vm_username
  custom_data = base64encode(templatefile("${path.module}/templates/bastion_user_data.sh.tpl", {
    key_vault_name = azurerm_key_vault.vault.name
    vm_username = local.vm_username
  }))

  admin_ssh_key {
    username   = local.vm_username
    public_key = jsondecode(azapi_resource_action.ssh_public_key_gen.output).publicKey
  }

  boot_diagnostics {
    storage_account_uri = azurerm_storage_account.storage_account.primary_blob_endpoint
  }

  identity {
    type = "SystemAssigned"
  }

}
