variable "domain_name" {
  description = "DNS domain name"
  type        = string
}

variable "project_name" {
  description = "Project name"
  type        = string
  default     = "infra"
}
