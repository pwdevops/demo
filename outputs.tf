output "site_url" {
  value = "www.${var.domain_name}"
}

output "bastion_host" {
  value = "bastion.${var.domain_name}"
}

output "load_balancer_ip_address" {
  value = azurerm_public_ip.lb_public_ip.ip_address
}

output "bastion_ip_address" {
  value = azurerm_public_ip.bastion_public_ip.ip_address
}

output "key_vault_name" {
  value = azurerm_key_vault.vault.name
}