locals {
  region           = "uksouth"
  vm_username      = "adminuser"
  db_username      = "mysql"
  db_schema        = var.project_name
  dns_ttl          = 600
  dns_cnames       = ["www", "admin"]
  backend_port     = 80
  frontend_port    = 80
  ssh_port         = 22
  bastion_ssh_port = 2234

  vm_config = {
    "vm_setup.sh.tpl" = "/usr/local/sbin"
  }
  vm_config_vars = {
    "vm_setup.sh.tpl" = {
      key_vault_name = azurerm_key_vault.vault.name
      project        = var.project_name
    }
  }
  vm_size             = "Standard_DS1_v2"
  vm_image_publisher  = "Canonical"
  vm_image_offer      = "0001-com-ubuntu-server-jammy"
  vm_image_sku        = "22_04-lts-gen2"
  vm_image_version    = "latest"
  vm_disk_caching     = "ReadWrite"
  vm_disk_acount_type = "Premium_LRS"
}