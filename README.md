# Azure Terraform Demo

## Description

This repo shows examples of using Azure with Terraform including creation of the following:

- A Key Vault with an access policy for use by a backend Ubuntu Linux virtual machine
- SSH Keys for virtual machine access stored in the Key Vault
- A MySQL database with its admin credentials and details stored in the Key Vault. A MySQL firewall rule allows access from a backend ubuntu host
- An Ubuntu bastion host with a public IP Address and templated user data that sets a custom ssh port. A network security group allowing access only on the custom ssh port
- A DNS Zone with A records and CNAME records
- Storage account, storage container and storage blobs for bootstrapping user data configuration
- A load balancer with a public IP address, a backend address pool, load balancer NAT rules and a backend Linux virtual machine. User data performs the following:
  - Installs Azure client and signs in with managed identity
  - Downloads configuration from storage container to bootstrap the instance
  - Installs various Linux tools such as nginx, network tools, MySQL client and mail server
  - Retrieves MySQL client login details and initialises a database and table if they don't already exist

## Building

Create a local environment.auto.tfvars with the following customised parameters

```
domain_name=example.com
```

This project is set to use repository http terraform state. To terraform init from Gitlab run the commands in the project repository at:

```Operate->Terraform states->default->Copy terraform init comand```

After this create with:

```
terraform apply
```

## Usage

The terraform will output various details that can be used, e.g.

```
bastion_host = "bastion.example.com"
bastion_ip_address = "2.3.4.5"
key_vault_name = "infra-abcdefghijklm"
load_balancer_ip_address = "1.2.3.4"
site_url = "www.example.com"
```

The nginx server can be viewed using the load balancer IP address given in the terraform output above, e.g.: http://<LOAD_BALANCER_IP_ADDRESS>

To ssh to the bastion using the IP address given in the terraform output above retrieve the private key from the key vault. For example using the az client with the key vault name and bastion ip adress from the terraform output e.g.:

```
az keyvault secret show --name "ssh-private-key" --vault-name "<KEY_VAULT_NAME>" --query "value" | sed 's/"//g' | sed 's/\\n/\n/g' | sed 's/\\r/\r/g' > infra.key
chmod 600 infra.key
ssh -i infra.key -p 2234 adminuser@<BASTION_IP_ADDRESS>
```