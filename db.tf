resource "random_password" "db_password" {
  length           = 16
  special          = true
  override_special = "!#$%&*()-_=+[]{}<>:?"
}

resource "azurerm_mysql_server" "mysql_server" {
  name                = "db-${var.project_name}-${random_id.unique_name_random_id.hex}"
  location            = azurerm_resource_group.rg.location
  resource_group_name = azurerm_resource_group.rg.name

  administrator_login          = local.db_username
  administrator_login_password = azurerm_key_vault_secret.vault_mysql_admin_password.value

  sku_name   = "B_Gen5_2"
  storage_mb = 5120
  version    = "8.0"

  auto_grow_enabled                 = true
  backup_retention_days             = 7
  geo_redundant_backup_enabled      = false
  infrastructure_encryption_enabled = false
  public_network_access_enabled     = true
  ssl_enforcement_enabled           = false
  ssl_minimal_tls_version_enforced  = "TLSEnforcementDisabled"

}

resource "azurerm_mysql_database" "mysql_db" {
  name                = local.db_schema
  resource_group_name = azurerm_resource_group.rg.name
  server_name         = azurerm_mysql_server.mysql_server.name
  charset             = "utf8"
  collation           = "utf8_unicode_ci"
}

resource "azurerm_mysql_firewall_rule" "mysql_fw_rule" {
  depends_on          = [azurerm_key_vault_access_policy.client_policy]
  name                = "vm-access"
  resource_group_name = azurerm_resource_group.rg.name
  server_name         = azurerm_mysql_server.mysql_server.name
  start_ip_address    = azurerm_public_ip.lb_public_ip.ip_address
  end_ip_address      = azurerm_public_ip.lb_public_ip.ip_address
}

resource "azurerm_key_vault_secret" "vault_mysql_hostname" {
  depends_on   = [azurerm_key_vault_access_policy.client_policy]
  name         = "mysql-hostname"
  value        = azurerm_mysql_server.mysql_server.fqdn
  key_vault_id = azurerm_key_vault.vault.id
}

resource "azurerm_key_vault_secret" "vault_mysql_db_name" {
  depends_on   = [azurerm_key_vault_access_policy.client_policy]
  name         = "mysql-db-name"
  value        = azurerm_mysql_database.mysql_db.name
  key_vault_id = azurerm_key_vault.vault.id
}

resource "azurerm_key_vault_secret" "vault_mysql_admin_username" {
  depends_on   = [azurerm_key_vault_access_policy.client_policy]
  name         = "mysql-admin-username"
  value        = "${local.db_username}@${azurerm_mysql_server.mysql_server.name}"
  key_vault_id = azurerm_key_vault.vault.id
}

resource "azurerm_key_vault_secret" "vault_mysql_admin_password" {
  depends_on   = [azurerm_key_vault_access_policy.client_policy]
  name         = "mysql-admin-pasword"
  value        = random_password.db_password.result
  key_vault_id = azurerm_key_vault.vault.id
}

