resource "azurerm_dns_zone" "zone" {
  name                = var.domain_name
  resource_group_name = azurerm_resource_group.rg.name
}

resource "azurerm_dns_a_record" "record" {
  name                = "@"
  resource_group_name = azurerm_resource_group.rg.name
  zone_name           = azurerm_dns_zone.zone.name
  ttl                 = local.dns_ttl
  records             = [azurerm_public_ip.lb_public_ip.ip_address]
}

resource "azurerm_dns_cname_record" "cname" {
  for_each = toset(local.dns_cnames)

  name                = each.key
  zone_name           = azurerm_dns_zone.zone.name
  resource_group_name = azurerm_resource_group.rg.name
  ttl                 = 300
  record              = azurerm_dns_zone.zone.name
}

resource "azurerm_dns_a_record" "bastion" {
  name                = "bastion"
  resource_group_name = azurerm_resource_group.rg.name
  zone_name           = azurerm_dns_zone.zone.name
  ttl                 = local.dns_ttl
  records             = [azurerm_public_ip.bastion_public_ip.ip_address]
}
