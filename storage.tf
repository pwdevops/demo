resource "azurerm_storage_account" "storage_account" {
  name                     = "${var.project_name}${random_id.unique_name_random_id.hex}"
  location                 = azurerm_resource_group.rg.location
  resource_group_name      = azurerm_resource_group.rg.name
  account_tier             = "Standard"
  account_replication_type = "LRS"
}

resource "azurerm_storage_container" "storage_container" {
  name                  = var.project_name
  storage_account_name  = azurerm_storage_account.storage_account.name
  container_access_type = "private"
}

data "azurerm_storage_account_sas" "sas_url_query_string" {
  connection_string = azurerm_storage_account.storage_account.primary_connection_string
  https_only        = true
  signed_version    = "2017-07-29"

  resource_types {
    service   = true
    container = true
    object    = true
  }

  services {
    blob  = true
    queue = false
    table = false
    file  = true
  }

  start  = "2023-12-01T00:00:00Z"
  expiry = "2030-03-21T00:00:00Z"

  permissions {
    read    = true
    write   = true
    delete  = false
    list    = true
    add     = true
    create  = true
    update  = false
    process = false
    filter  = false
    tag     = false
  }
}

resource "azurerm_key_vault_secret" "vault_sas_url_query_string" {
  depends_on   = [azurerm_key_vault_access_policy.client_policy]
  name         = "sas-url-query-string"
  value        = data.azurerm_storage_account_sas.sas_url_query_string.sas
  key_vault_id = azurerm_key_vault.vault.id
}

resource "azurerm_storage_blob" "vm_config_storage_blob" {
  for_each = local.vm_config

  name                   = "vm_config/${trimsuffix(each.key, ".tpl")}"
  storage_account_name   = azurerm_storage_account.storage_account.name
  storage_container_name = azurerm_storage_container.storage_container.name
  type                   = "Block"
  source_content         = templatefile("${path.module}/templates/vm_config/${each.key}", local.vm_config_vars[each.key])
}
